import sys
import json
import socketserver
import time

#python3 serversip.py 6001

IP_SIP = '127.0.0.1'

def get_time():
    # para los historicos 
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f'{year}{month}{day}{hour}{minute}{second}'

def get_arguments():
    try:
        port = int(sys.argv[1]) #6001
    except:
        sys.exit('Introduzca: python3 serversip.py <port>')
    return port

def there_is_dicc():#leemos el dicc
    users_dict = {}
    try:
        fich = open('registrar.json', 'r')
        data = json.loads(fich.read())
        fich.close()
        for user in data:
            for key, value in user.items():
                users_dict[key] = value  # clave: valor ejemplo del enunciado "sip:servidor1@songs.net": "sip:servidor1@127.0.0.1:20354",
    except ValueError:
        pass

    return users_dict

class SIPHandler(socketserver.BaseRequestHandler):
    usersDict = there_is_dicc()

    def process_register(self, sip, real):
        # Registrar usuarios
        self.usersDict[sip] = real  # Actualizo diccionario
        # Actualizo archivo registrar.json
        fich = open('registrar.json', 'w')
        fich.write(str(json.dumps([self.usersDict], indent=1)))
        fich.close()


    def search_address(self, sip_address):
        # Busca direcciones en el diccionario
        try:
            return self.usersDict[sip_address]
        except ValueError:
            return 'User Not Found'    # Si no lo encuentro mando este error
 
    #---------PROCESAMIENTO DE LOS DATOS-------
    def handle(self):
        metodos_validos = ("REGISTER", "INVITE", "ACK", "BYE")
        data = self.request[0]
        sock = self.request[1]
        received = data.decode('utf-8') 
        # REGISTER sip:servidor1@songs.net SIP/2.0
        # REGISTER sip:cliente1@clientes.net SIP/2.0
        # INVITE sip:servidor1@songs.net SIP/2.0
        # ACK sip:servidor1@songs.net SIP/2.0
        # ACK sip:servidor1@127.0.0.1:20555 SIP/2.0

        petition = received.split()[0] #REGISTER / INVITE / ACK
        print(f'{get_time()} SIP from {self.client_address[0]}:{self.client_address[1]}:\r\n{data.decode("utf-8")}')
        
        if type(received) is not str:
            response = 'SIP/2.0 400 Bad Request'
            sock.sendto(response.encode('utf-8'), self.client_address)
        
        elif petition not in metodos_validos:
            response = 'SIP/2.0 405 Method Not Allowed'
            sock.sendto(response.encode('utf-8'), self.client_address)

        # Register
        elif petition == metodos_validos[0]: 
            sip = received.split()[1] 
            real = f'{sip.split("@")[0]}@{self.client_address[0]}:{self.client_address[1]}' 
            self.process_register(sip, real)  

            response = f'SIP/2.0 200 OK\r\n\r\n' #enunciado dice que respondera con un OK, sin cabecera
            sock.sendto(response.encode('utf-8'), self.client_address)
            print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}:\r\n{response}')  # Histórico

        # invite
        elif petition == metodos_validos[1]: 
            user = self.search_address(received.split()[1])
            if user == 'User Not Found':
                response = f'SIP/2.0 404 User Not Found\r\n\r\n'
                sock.sendto(response.encode('utf-8'), self.client_address)
                print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}:\r\n{response}')  # Histórico
                
            else:
                response = f'SIP/2.0 302 Moved Temporarily\r\nContact: {user}\r\n\r\n' 
                sock.sendto(response.encode('utf-8'), self.client_address)
                print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}:\r\n{response}')  # Histórico

        # ack
        elif petition == metodos_validos[2]:
            '''print(received.split('\r\n'))
            sip = received.split('\r\n')[4].split()[0].split('=')[1]
            print(f'SIP: {sip}')'''

def main():
    port = get_arguments()

    try:
        serv = socketserver.UDPServer((IP_SIP, port), SIPHandler)
        print(f'{get_time()} Starting...')  # Histórico 
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")


    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Servidor finalizado")
        sys.exit(0)

if __name__ == '__main__':
    main()
