#!/usr/bin/env python

import sys
import socket
import threading
import time

import simplertp
#python3 client.py 127.0.0.1:6001 sip:cliente1@clientes.net sip:servidor1@songs.net cancion.mp3

#----VALORES-----  
IP = '127.0.0.1'
PORT = 11111 #puerto desde donde enviaremos los paquetes UDP
#------TIEMPO----- 
def get_time():
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f'{year}{month}{day}{hour}{minute}{second}'

#-----------conseguir los argumentos---------
def conseguir_argv():
    try:
        IpServerSIP = sys.argv[1].split(':')[0] #partimos en : y nos quedamos con el primero, IP
        portServerSIP = int(sys.argv[1].split(':')[1]) #partimos en : y nos quedamos con el segungo
        addclient = sys.argv[2] #direccion del cliente
        addServer = sys.argv[3] #direccion del servidor
        file = sys.argv[4]
    except:
        sys.exit('Usage: python3 client.py <IPServerSIP>:<portServidorSIP> <addClient> <addServerRTP> <file>')
    return IpServerSIP, portServerSIP, addclient, addServer, file
#-----------MAIN ------------
def main():
    IpServerSIP, portServerSIP, addclient, addServer, file = conseguir_argv()

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            print(f'{get_time()} Starting...')  # Histórico 
            register = f'REGISTER {addclient} SIP/2.0\r\n\r\n'
            my_socket.sendto(register.encode('utf-8'), (IpServerSIP, portServerSIP))
            print(f'{get_time()} SIP to {IpServerSIP}:{portServerSIP}: {register}')  # Histórico

            data, address = my_socket.recvfrom(1024)
            mssg = data.decode('utf-8')
            print(f'{get_time()} SIP from {address[0]}:{address[1]}: {mssg}')
     
            if mssg == 'SIP/2.0 200 OK\r\n\r\n': 
                protocol = f'INVITE {addServer} SIP/2.0' 
                From = f'From: <{addclient}>'
                content_type = 'Content-Type: application/sdp'
                
                session_name = addclient.split("@")[0].split(":")[1] #s=cliente1
                sdp_elements = f'v=0\r\no={addclient} 127.0.0.1\r\ns={session_name}\r\nt=0\r\nm=audio {PORT} RTP'
                message = f'{protocol}\r\n{From}\r\n{content_type}\r\n\r\n{sdp_elements}'
                my_socket.sendto(message.encode('utf-8'), (IpServerSIP, portServerSIP)) # lo que enviamos al servidor SIP
                print(f'{get_time()} SIP to {address[0]}:{address[1]}: {message}')

                data2, address = my_socket.recvfrom(1024) 
                mssg2 = data2.decode('utf-8')
                print(f'{get_time()} SIP from {address[0]}:{address[1]}:\r\n{mssg2}')

           
                if data2.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 302 Moved Temporarily':
                    global nueva_dic_IP
                    global nueva_dic_PORT
                    #SIP/2.0 302 Moved Temporarily\r\nContact: sip:servidor1@127.0.0.1:20354 (donde es espera recibir los paquetes)
                    nueva_dic = data2.decode('utf-8').split('\r\n')[1].split()[1] #sip:server1@127.0.0.1:20354 
                    nueva_dic_IP = nueva_dic.split('@')[1].split(':')[0]#127.0.0.1
                    nueva_dic_PORT = int(nueva_dic.split('@')[1].split(':')[1])#20354

                    ack = f'ACK {addServer} SIP/2.0' #debe enviar servidor1
                    From = f'From: <{addclient}>\r\n\r\n'
                    message1 = f'{ack}\r\n{From}'
                    my_socket.sendto(message1.encode('utf-8'), (IpServerSIP, portServerSIP))
                    print(f'{get_time()} SIP to {IpServerSIP}:{portServerSIP}:\r\n{message1}')
                   
                    #------INVITE a la nueva direccion IP------
                    protocol = f'INVITE {nueva_dic} SIP/2.0'  #sip:servidor1@127.0.0.1:20354
                    From = f'From: <{addclient}>' #sip:cliente1@clientes.net
                    content_type = 'Content-Type: application/sdp'

                    session_name = addclient.split("@")[0].split(":")[1] 
                    sdp_elements = f'v=0\r\no={addclient} 127.0.0.1\r\ns={session_name}\r\nt=0\r\nm=audio {PORT} RTP' #puerto desde donde enviaremos los RTP
                    message = f'{protocol}\r\n{From}\r\n{content_type}\r\n\r\n{sdp_elements}'
                    my_socket.sendto(message.encode('utf-8'), (nueva_dic_IP, nueva_dic_PORT)) # enviamos el invite a la dirrecion 192.169... 53001
                    print(f'{get_time()} SIP to {nueva_dic_IP}:{nueva_dic_PORT}:\r\n{message}')
                    
                    data, address = my_socket.recvfrom(1024)
                    mensaje = data.decode('utf-8')
                    confirmacion = mensaje.split('\r\n')[0]
                    print(f'{get_time()} SIP from {address[0]}:{address[1]}:\r\n{data.decode("utf-8")}')
                    #el cliente se quedara esperando el OK del servidorRTP y cuando lo reciba enviara un ack al sip
                    if confirmacion == 'SIP/2.0 200 OK':
                        ack = f'ACK {nueva_dic} SIP/2.0'
                        From = f'From: <{addclient}>\r\n\r\n'
                        message = f'{ack}\r\n{From}'
                        my_socket.sendto(message.encode('utf-8'), (IpServerSIP, portServerSIP)) 
                        print(f'{get_time()} SIP to {IpServerSIP}:{portServerSIP}:\r\n{message}')

                        #--------ENVIAR PAQUETES RTP--al puerto indicado en el documento SDP------
                        port_client= mensaje.split("m=audio ")[1].split(' ')[0]
                        audio_file = sys.argv[4]
                        print(f"RTP to {nueva_dic_IP}:{nueva_dic_PORT}")
                        sender = simplertp.RTPSender(ip=nueva_dic_IP, port=int(port_client), file=audio_file, printout=True)  #IP Y PORT DEL RTP
                        sender.send_threaded()
                        time.sleep(30)
                        print(" Finalizando el thread de envío. ")
                        sender.finish()

                        print(f'---CERRAMOS ENVIO PAQUETES RTP---')
                        #BYE sip:servidor1@s@192.168.10.16:53001 SIP/2.0\r\n\r\n
                        server1 = nueva_dic.split('@')[0] #sip:server1
                        message = f'BYE {server1}@{nueva_dic_IP}:{nueva_dic_PORT} SIP/2.0\r\n\r\n' 
                        my_socket.sendto(message.encode('utf-8'), (nueva_dic_IP, nueva_dic_PORT))
                        print(f'{get_time()} SIP to {nueva_dic_IP}:{nueva_dic_PORT}:\r\n{message}')
                       
                        data = my_socket.recv(1024) #el OK DEL SERVER rtp
                        #Cuando reciba el OK del serverRTp TERMINARA
                        mensajertp = data.decode('utf-8')
                        ok_rtp = mensajertp.split('\r\n')[0]
                        if ok_rtp == 'SIP/2.0 200 OK':
                            print("------Server done-------")
                            my_socket.close()

    except ConnectionRefusedError:
        print("Error conectando a servidor")

if __name__ == '__main__':
    main()

